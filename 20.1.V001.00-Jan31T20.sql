-- version : 20.1.V001.00  31st Jan,2020

-- insert email templates for employee and supervisor evaluation Sharmistha start

INSERT INTO `tedpros_email_template`(`id`, `templateFor`, `template`) VALUES ('','EmployerEvaluation','<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office"
    xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <meta content="width=device-width" name="viewport" />
    <!--[if !mso]><!-->
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <!--<![endif]-->
    <title></title>
    <!--[if !mso]><!-->
    <!--<![endif]-->
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
        }

        table,
        td,
        tr {
            vertical-align: top;
            border-collapse: collapse;
        }

        * {
            line-height: inherit;
        }

        a[x-apple-data-detectors=true] {
            color: inherit !important;
            text-decoration: none !important;
        }

        .ie-browser table {
            table-layout: fixed;
        }

        [owa] .img-container div,
        [owa] .img-container button {
            display: block !important;
        }

        [owa] .fullwidth button {
            width: 100% !important;
        }

        [owa] .block-grid .col {
            display: table-cell;
            float: none !important;
            vertical-align: top;
        }

        .ie-browser .block-grid,
        .ie-browser .num12,
        [owa] .num12,
        [owa] .block-grid {
            width: 600px !important;
        }

        .ie-browser .mixed-two-up .num4,
        [owa] .mixed-two-up .num4 {
            width: 200px !important;
        }

        .ie-browser .mixed-two-up .num8,
        [owa] .mixed-two-up .num8 {
            width: 400px !important;
        }

        .ie-browser .block-grid.two-up .col,
        [owa] .block-grid.two-up .col {
            width: 300px !important;
        }

        .ie-browser .block-grid.three-up .col,
        [owa] .block-grid.three-up .col {
            width: 300px !important;
        }

        .ie-browser .block-grid.four-up .col [owa] .block-grid.four-up .col {
            width: 150px !important;
        }

        .ie-browser .block-grid.five-up .col [owa] .block-grid.five-up .col {
            width: 120px !important;
        }

        .ie-browser .block-grid.six-up .col,
        [owa] .block-grid.six-up .col {
            width: 100px !important;
        }

        .ie-browser .block-grid.seven-up .col,
        [owa] .block-grid.seven-up .col {
            width: 85px !important;
        }

        .ie-browser .block-grid.eight-up .col,
        [owa] .block-grid.eight-up .col {
            width: 75px !important;
        }

        .ie-browser .block-grid.nine-up .col,
        [owa] .block-grid.nine-up .col {
            width: 66px !important;
        }

        .ie-browser .block-grid.ten-up .col,
        [owa] .block-grid.ten-up .col {
            width: 60px !important;
        }

        .ie-browser .block-grid.eleven-up .col,
        [owa] .block-grid.eleven-up .col {
            width: 54px !important;
        }

        .ie-browser .block-grid.twelve-up .col,
        [owa] .block-grid.twelve-up .col {
            width: 50px !important;
        }
    </style>
    <style id="media-query" type="text/css">
        @media only screen and (min-width: 620px) {
            .block-grid {
                width: 600px !important;
            }

            .block-grid .col {
                vertical-align: top;
            }

            .block-grid .col.num12 {
                width: 600px !important;
            }

            .block-grid.mixed-two-up .col.num3 {
                width: 150px !important;
            }

            .block-grid.mixed-two-up .col.num4 {
                width: 200px !important;
            }

            .block-grid.mixed-two-up .col.num8 {
                width: 400px !important;
            }

            .block-grid.mixed-two-up .col.num9 {
                width: 450px !important;
            }

            .block-grid.two-up .col {
                width: 300px !important;
            }

            .block-grid.three-up .col {
                width: 200px !important;
            }

            .block-grid.four-up .col {
                width: 150px !important;
            }

            .block-grid.five-up .col {
                width: 120px !important;
            }

            .block-grid.six-up .col {
                width: 100px !important;
            }

            .block-grid.seven-up .col {
                width: 85px !important;
            }

            .block-grid.eight-up .col {
                width: 75px !important;
            }

            .block-grid.nine-up .col {
                width: 66px !important;
            }

            .block-grid.ten-up .col {
                width: 60px !important;
            }

            .block-grid.eleven-up .col {
                width: 54px !important;
            }

            .block-grid.twelve-up .col {
                width: 50px !important;
            }
        }

        @media (max-width: 620px) {

            .block-grid,
            .col {
                min-width: 320px !important;
                max-width: 100% !important;
                display: block !important;
            }

            .block-grid {
                width: 100% !important;
            }

            .col {
                width: 100% !important;
            }

            .col>div {
                margin: 0 auto;
            }

            img.fullwidth,
            img.fullwidthOnMobile {
                max-width: 100% !important;
            }

            .no-stack .col {
                min-width: 0 !important;
                display: table-cell !important;
            }

            .no-stack.two-up .col {
                width: 50% !important;
            }

            .no-stack .col.num4 {
                width: 33% !important;
            }

            .no-stack .col.num8 {
                width: 66% !important;
            }

            .no-stack .col.num4 {
                width: 33% !important;
            }

            .no-stack .col.num3 {
                width: 25% !important;
            }

            .no-stack .col.num6 {
                width: 50% !important;
            }

            .no-stack .col.num9 {
                width: 75% !important;
            }

            .mobile_hide {
                min-height: 0px;
                max-height: 0px;
                max-width: 0px;
                display: none;
                overflow: hidden;
                font-size: 0px;
            }
        }
    </style>
</head>

<body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #283C4B;">
    <style id="media-query-bodytag" type="text/css">
        @media (max-width: 620px) {
            .block-grid {
                min-width: 320px !important;
                max-width: 100% !important;
                width: 100% !important;
                display: block !important;
            }

            .col {
                min-width: 320px !important;
                max-width: 100% !important;
                width: 100% !important;
                display: block !important;
            }

            .col>div {
                margin: 0 auto;
            }

            img.fullwidth {
                max-width: 100% !important;
                height: auto !important;
            }

            img.fullwidthOnMobile {
                max-width: 100% !important;
                height: auto !important;
            }

            .no-stack .col {
                min-width: 0 !important;
                display: table-cell !important;
            }

            .no-stack.two-up .col {
                width: 50% !important;
            }

            .no-stack.mixed-two-up .col.num4 {
                width: 33% !important;
            }

            .no-stack.mixed-two-up .col.num8 {
                width: 66% !important;
            }

            .no-stack.three-up .col.num4 {
                width: 33% !important
            }

            .no-stack.four-up .col.num3 {
                width: 25% !important
            }
        }
    </style>
    <table bgcolor="#283C4B" cellpadding="0" cellspacing="0" class="nl-container"
        style="table-layout: fixed; vertical-align: top; min-width: 320px; margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #283C4B; width: 100%;"
        valign="top" width="100%">
        <tbody>
            <tr style="vertical-align: top;" valign="top">
                <td style="word-break: break-word; vertical-align: top; border-collapse: collapse;" valign="top">
                  
                    <div style="background-color:#283C4B;">

                    
                                         
                                  
                        <div class="block-grid"
                            style="Margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #3AAEE0;;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#3AAEE0;">
                                <div class="col num12"
                                    style="min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;;">
                                    <div style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                            <div
                                                style="color:#FFFFFF;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:120%;padding-top:30px;padding-right:20px;padding-bottom:20px;padding-left:20px;">
                                                <div
                                                    style="font-size: 12px; line-height: 14px; color: #FFFFFF; font-family: Arial, Helvetica Neue, Helvetica, sans-serif;text-align: -webkit-center;">
                                                    <p
                                                        style="font-size: 18px; line-height: 28px; text-align: center; margin: 10px;padding:10px;text-align: -webkit-center;background-color: #fff; padding: 11px;  border-radius: 7px; width:200px;">
                                                     
 <img align="center" alt="Image" border="0" class="center autowidth"
                                                        src="backEnd companyLogo"
                                                        style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; clear: both; border: 0; height: auto; float: none; width: 100%; display: block;min-width:120px;max-width:180px;border-radius:5px;"
                                                        title="Image" width="100" />

                                                    </p>
                                                </div>
                                            </div>
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="background-color:#283C4B;">
                        <div class="block-grid"
                            style="Margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #FFFFFF;;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
                                <div class="col num12"
                                    style="min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;">
                                    <div style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:15px; padding-right: 0px; padding-left: 0px;">
                                            <div
                                                style="color:#283C4B;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:150%;padding-top:10px;padding-right:30px;padding-bottom:10px;padding-left:30px;">
                                                <div
                                                    style="font-size: 12px; line-height: 18px; color: #283C4B; font-family: Arial, Helvetica Neue, Helvetica, sans-serif;">
                                                    <p style="font-size: 12px; line-height: 24px; margin: 0;"><span
                                                            style="font-size: 16px;"><strong><span
                                                                    style="font-family: lucida sans unicode, lucida grande, sans-serif; line-height: 24px; font-size: 16px;">Hi empfirstname emplastname ,</span></strong> </span> </p><br><br>
                                                    <p style="font-size: 12px; line-height: 21px; margin: 0;"><span
                                                            style="font-family: lucida sans unicode, lucida grande, sans-serif; font-size: 14px;">Please find your Supervisors final Review for the period .


                                            </span>
                                                    </p>

                                                    
                                                </div>
                                            </div>
                                           
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="block-grid"
                            style="Margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #FFFFFF;;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
                                <div class="col num12"
                                    style="min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;">
                                    <div style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:15px; padding-right: 0px; padding-left: 0px;">
                                            <div
                                                style="color:#283C4B;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:150%;padding-top:10px;padding-right:30px;padding-bottom:10px;padding-left:30px;">
                                                <div
                                                    style="font-size: 12px; line-height: 18px; color: #283C4B; font-family: Arial, Helvetica Neue, Helvetica, sans-serif;">
                                                    <p style="font-size: 12px; line-height: 24px; margin: 0;"><span
                                                            style="font-size: 16px;"><strong><span
                                                                    style="font-family: lucida sans unicode, lucida grande, sans-serif; line-height: 24px; font-size: 16px;">

                                                                
                                                          Best Wishes,<br>
                                                        Team HR</span></strong> </span> </p>
                                                    <p style="font-size: 12px; line-height: 21px; margin: 0;"><span
                                                            style="font-family: lucida sans unicode, lucida grande, sans-serif; color:#9a9393;">
                                                            This is an automated email .                                                         </span>
                                                    </p><br>
                                                   
                                                </div>
                                            </div>
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>






                       
                    <div style="background-color:#283C4B;">
                        <div class="block-grid"
                            style="margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;;">
                            <div
                                style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <div class="col num12"
                                    style="min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;;">
                                    <div style="width:100% !important;">
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-bottom:10px; padding-right: 0px; padding-left: 0px;">


                                             <div
                                                style="color:#283C4B;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:150%;padding-top:10px;padding-right:30px;padding-bottom:0px;padding-left:30px;">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>');




INSERT INTO `tedpros_email_template`(`id`, `templateFor`, `template`) VALUES ('','SupervisorEvaluation','<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office"
    xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <meta content="width=device-width" name="viewport" />
    <!--[if !mso]><!-->
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <!--<![endif]-->
    <title></title>
    <!--[if !mso]><!-->
    <!--<![endif]-->
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
        }

        table,
        td,
        tr {
            vertical-align: top;
            border-collapse: collapse;
        }

        * {
            line-height: inherit;
        }

        a[x-apple-data-detectors=true] {
            color: inherit !important;
            text-decoration: none !important;
        }

        .ie-browser table {
            table-layout: fixed;
        }

        [owa] .img-container div,
        [owa] .img-container button {
            display: block !important;
        }

        [owa] .fullwidth button {
            width: 100% !important;
        }

        [owa] .block-grid .col {
            display: table-cell;
            float: none !important;
            vertical-align: top;
        }

        .ie-browser .block-grid,
        .ie-browser .num12,
        [owa] .num12,
        [owa] .block-grid {
            width: 600px !important;
        }

        .ie-browser .mixed-two-up .num4,
        [owa] .mixed-two-up .num4 {
            width: 200px !important;
        }

        .ie-browser .mixed-two-up .num8,
        [owa] .mixed-two-up .num8 {
            width: 400px !important;
        }

        .ie-browser .block-grid.two-up .col,
        [owa] .block-grid.two-up .col {
            width: 300px !important;
        }

        .ie-browser .block-grid.three-up .col,
        [owa] .block-grid.three-up .col {
            width: 300px !important;
        }

        .ie-browser .block-grid.four-up .col [owa] .block-grid.four-up .col {
            width: 150px !important;
        }

        .ie-browser .block-grid.five-up .col [owa] .block-grid.five-up .col {
            width: 120px !important;
        }

        .ie-browser .block-grid.six-up .col,
        [owa] .block-grid.six-up .col {
            width: 100px !important;
        }

        .ie-browser .block-grid.seven-up .col,
        [owa] .block-grid.seven-up .col {
            width: 85px !important;
        }

        .ie-browser .block-grid.eight-up .col,
        [owa] .block-grid.eight-up .col {
            width: 75px !important;
        }

        .ie-browser .block-grid.nine-up .col,
        [owa] .block-grid.nine-up .col {
            width: 66px !important;
        }

        .ie-browser .block-grid.ten-up .col,
        [owa] .block-grid.ten-up .col {
            width: 60px !important;
        }

        .ie-browser .block-grid.eleven-up .col,
        [owa] .block-grid.eleven-up .col {
            width: 54px !important;
        }

        .ie-browser .block-grid.twelve-up .col,
        [owa] .block-grid.twelve-up .col {
            width: 50px !important;
        }
    </style>
    <style id="media-query" type="text/css">
        @media only screen and (min-width: 620px) {
            .block-grid {
                width: 600px !important;
            }

            .block-grid .col {
                vertical-align: top;
            }

            .block-grid .col.num12 {
                width: 600px !important;
            }

            .block-grid.mixed-two-up .col.num3 {
                width: 150px !important;
            }

            .block-grid.mixed-two-up .col.num4 {
                width: 200px !important;
            }

            .block-grid.mixed-two-up .col.num8 {
                width: 400px !important;
            }

            .block-grid.mixed-two-up .col.num9 {
                width: 450px !important;
            }

            .block-grid.two-up .col {
                width: 300px !important;
            }

            .block-grid.three-up .col {
                width: 200px !important;
            }

            .block-grid.four-up .col {
                width: 150px !important;
            }

            .block-grid.five-up .col {
                width: 120px !important;
            }

            .block-grid.six-up .col {
                width: 100px !important;
            }

            .block-grid.seven-up .col {
                width: 85px !important;
            }

            .block-grid.eight-up .col {
                width: 75px !important;
            }

            .block-grid.nine-up .col {
                width: 66px !important;
            }

            .block-grid.ten-up .col {
                width: 60px !important;
            }

            .block-grid.eleven-up .col {
                width: 54px !important;
            }

            .block-grid.twelve-up .col {
                width: 50px !important;
            }
        }

        @media (max-width: 620px) {

            .block-grid,
            .col {
                min-width: 320px !important;
                max-width: 100% !important;
                display: block !important;
            }

            .block-grid {
                width: 100% !important;
            }

            .col {
                width: 100% !important;
            }

            .col>div {
                margin: 0 auto;
            }

            img.fullwidth,
            img.fullwidthOnMobile {
                max-width: 100% !important;
            }

            .no-stack .col {
                min-width: 0 !important;
                display: table-cell !important;
            }

            .no-stack.two-up .col {
                width: 50% !important;
            }

            .no-stack .col.num4 {
                width: 33% !important;
            }

            .no-stack .col.num8 {
                width: 66% !important;
            }

            .no-stack .col.num4 {
                width: 33% !important;
            }

            .no-stack .col.num3 {
                width: 25% !important;
            }

            .no-stack .col.num6 {
                width: 50% !important;
            }

            .no-stack .col.num9 {
                width: 75% !important;
            }

            .mobile_hide {
                min-height: 0px;
                max-height: 0px;
                max-width: 0px;
                display: none;
                overflow: hidden;
                font-size: 0px;
            }
        }
    </style>
</head>

<body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #283C4B;">
    <style id="media-query-bodytag" type="text/css">
        @media (max-width: 620px) {
            .block-grid {
                min-width: 320px !important;
                max-width: 100% !important;
                width: 100% !important;
                display: block !important;
            }

            .col {
                min-width: 320px !important;
                max-width: 100% !important;
                width: 100% !important;
                display: block !important;
            }

            .col>div {
                margin: 0 auto;
            }

            img.fullwidth {
                max-width: 100% !important;
                height: auto !important;
            }

            img.fullwidthOnMobile {
                max-width: 100% !important;
                height: auto !important;
            }

            .no-stack .col {
                min-width: 0 !important;
                display: table-cell !important;
            }

            .no-stack.two-up .col {
                width: 50% !important;
            }

            .no-stack.mixed-two-up .col.num4 {
                width: 33% !important;
            }

            .no-stack.mixed-two-up .col.num8 {
                width: 66% !important;
            }

            .no-stack.three-up .col.num4 {
                width: 33% !important
            }

            .no-stack.four-up .col.num3 {
                width: 25% !important
            }
        }
    </style>
    <table bgcolor="#283C4B" cellpadding="0" cellspacing="0" class="nl-container"
        style="table-layout: fixed; vertical-align: top; min-width: 320px; margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #283C4B; width: 100%;"
        valign="top" width="100%">
        <tbody>
            <tr style="vertical-align: top;" valign="top">
                <td style="word-break: break-word; vertical-align: top; border-collapse: collapse;" valign="top">
                  
                    <div style="background-color:#283C4B;">

                    
                                         
                                  
                        <div class="block-grid"
                            style="Margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #3AAEE0;;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#3AAEE0;">
                                <div class="col num12"
                                    style="min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;;">
                                    <div style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                            <div
                                                style="color:#FFFFFF;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:120%;padding-top:30px;padding-right:20px;padding-bottom:20px;padding-left:20px;">
                                                <div
                                                    style="font-size: 12px; line-height: 14px; color: #FFFFFF; font-family: Arial, Helvetica Neue, Helvetica, sans-serif;text-align: -webkit-center;">
                                                    <p
                                                        style="font-size: 18px; line-height: 28px; text-align: center; margin: 10px;padding:10px;text-align: -webkit-center;background-color: #fff; padding: 11px;  border-radius: 7px; width:200px;">
                                                     
 <img align="center" alt="Image" border="0" class="center autowidth"
                                                        src="backEnd companyLogo"
                                                        style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; clear: both; border: 0; height: auto; float: none; width: 100%; display: block;min-width:120px;max-width:180px;border-radius:5px;"
                                                        title="Image" width="100" />

                                                    </p>
                                                </div>
                                            </div>
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="background-color:#283C4B;">
                        <div class="block-grid"
                            style="Margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #FFFFFF;;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
                                <div class="col num12"
                                    style="min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;">
                                    <div style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:15px; padding-right: 0px; padding-left: 0px;">
                                            <div
                                                style="color:#283C4B;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:150%;padding-top:10px;padding-right:30px;padding-bottom:10px;padding-left:30px;">
                                                <div
                                                    style="font-size: 12px; line-height: 18px; color: #283C4B; font-family: Arial, Helvetica Neue, Helvetica, sans-serif;">
                                                    <p style="font-size: 12px; line-height: 24px; margin: 0;"><span
                                                            style="font-size: 16px;"><strong><span
                                                                    style="font-family: lucida sans unicode, lucida grande, sans-serif; line-height: 24px; font-size: 16px;">Hi firstName lastName ,</span></strong> </span> </p><br><br>
                                                    <p style="font-size: 12px; line-height: 21px; margin: 0;"><span
                                                            style="font-family: lucida sans unicode, lucida grande, sans-serif; font-size: 14px;">                                                    
                                                             Your empfirstname emplastname, have submitted his/her Performance Index Inputs for your approval for the work_start_date to  due_date. To complete the rating process, give your valuable Inputs and Comments.

                                                               </span>
                                                    </p><br>

                                                   
                                                    
                                                </div>
                                            </div>
                                           
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="block-grid"
                            style="Margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #FFFFFF;;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
                                <div class="col num12"
                                    style="min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;">
                                    <div style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:15px; padding-right: 0px; padding-left: 0px;">
                                            <div
                                                style="color:#283C4B;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:150%;padding-top:10px;padding-right:30px;padding-bottom:10px;padding-left:30px;">
                                                <div
                                                    style="font-size: 12px; line-height: 18px; color: #283C4B; font-family: Arial, Helvetica Neue, Helvetica, sans-serif;">
                                                    <p style="font-size: 12px; line-height: 24px; margin: 0;"><span
                                                            style="font-size: 16px;"><strong><span
                                                                    style="font-family: lucida sans unicode, lucida grande, sans-serif; line-height: 24px; font-size: 16px;">

                                                               
                                                           
                                                       Best Wishes</span></strong> </span> </p>
                                                    <p style="font-size: 12px; line-height: 21px; margin: 0;"><span
                                                            style="font-family: lucida sans unicode, lucida grande, sans-serif; color:#9a9393;">
                                                            This is an automated email .                                                         </span>
                                                    </p><br>
                                                   
                                                </div>
                                            </div>
                                          
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>






                       
                    <div style="background-color:#283C4B;">
                        <div class="block-grid"
                            style="margin: 0 auto; min-width: 320px; max-width: 600px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;;">
                            <div
                                style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <div class="col num12"
                                    style="min-width: 320px; max-width: 600px; display: table-cell; vertical-align: top;;">
                                    <div style="width:100% !important;">
                                        <div
                                            style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-bottom:10px; padding-right: 0px; padding-left: 0px;">


                                             <div
                                                style="color:#283C4B;font-family:Arial, Helvetica Neue, Helvetica, sans-serif;line-height:150%;padding-top:10px;padding-right:30px;padding-bottom:0px;padding-left:30px;">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>');

-- insert email templates for employee and supervisor evaluation Sharmistha end

-- insert email templates for vendor email notification while adding the job evaluation Suresh start
INSERT INTO `tedpros_email_template` (`id`, `templateFor`, `template`) VALUES ('26', 'vendorEmployeeEmails', '<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n\r\n<head>\r\n	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\r\n	<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n	<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">\r\n	<meta name=\"format-detection\" content=\"telephone=no\" />\r\n	<!-- disable auto telephone linking in iOS -->\r\n	<title>Job Notification</title>\r\n	<style type=\"text/css\">\r\n		/* RESET STYLES */\r\n		html { background-color:#E1E1E1; margin:0; padding:0; }\r\n		body, #bodyTable, #bodyCell, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;font-family:Helvetica, Arial, \"Lucida Grande\", sans-serif;}\r\n		table{border-collapse:collapse;}\r\n		table[id=bodyTable] {width:100%!important;margin:auto;max-width:500px!important;color:#7A7A7A;font-weight:normal;}\r\n		img, a img{border:0; outline:none; text-decoration:none;height:auto; line-height:100%;}\r\n		a {text-decoration:none !important;border-bottom: 1px solid;}\r\n		h1, h2, h3, h4, h5, h6{color:#5F5F5F; font-weight:normal; font-family:Helvetica; font-size:20px; line-height:125%; text-align:Left; letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;}\r\n	\r\n		/* CLIENT-SPECIFIC STYLES */\r\n		.ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail/Outlook.com to display emails at full width. */\r\n		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;} /* Force Hotmail/Outlook.com to display line heights normally. */\r\n		table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up. */\r\n		#outlook a{padding:0;} /* Force Outlook 2007 and up to provide a \"view in browser\" message. */\r\n		img{-ms-interpolation-mode: bicubic;display:block;outline:none; text-decoration:none;} /* Force IE to smoothly render resized images. */\r\n		body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; font-weight:normal!important;} /* Prevent Windows- and Webkit-based mobile platforms from changing declared text sizes. */\r\n		.ExternalClass td[class=\"ecxflexibleContainerBox\"] h3 {padding-top: 10px !important;} /* Force hotmail to push 2-grid sub headers down */\r\n	\r\n		/* /\\/\\/\\/\\/\\/\\/\\/\\/ TEMPLATE STYLES /\\/\\/\\/\\/\\/\\/\\/\\/ */\r\n	\r\n		/* ========== Page Styles ========== */\r\n		h1{display:block;font-size:26px;font-style:normal;font-weight:normal;line-height:100%;}\r\n	 h2{display:block;font-size:20px;font-style:normal;font-weight:normal;line-height:120%;}\r\n		h3{display:block;font-size:17px;font-style:normal;font-weight:normal;line-height:110%;}\r\n		h4{display:block;font-size:18px;font-style:italic;font-weight:normal;line-height:100%;}\r\n		.flexibleImage{height:auto;}\r\n		.linkRemoveBorder{border-bottom:0 !important;}\r\n		table[class=flexibleContainerCellDivider] {padding-bottom:0 !important;padding-top:0 !important;}\r\n	\r\n		body, #bodyTable{background-color:#E1E1E1;}\r\n		#emailHeader{background-color:#E1E1E1;}\r\n		#emailBody{background-color:#FFFFFF;}\r\n		#emailFooter{background-color:#E1E1E1;}\r\n		.nestedContainer{background-color:#F8F8F8; border:1px solid #CCCCCC;}\r\n	 .emailButton{color:rgba(0,0,0,0.0.33); border-collapse:separate;font-weight: bold;font-size:18px;}\r\n		.buttonContent{color:#FFFFFF; font-family:Helvetica; font-size:18px; font-weight:bold; line-height:100%; padding:15px; text-align:center;}\r\n		.buttonContent a{color:#FFFFFF; display:block; text-decoration:none!important; border:0!important;}\r\n		.emailCalendar{background-color:#FFFFFF; border:1px solid #CCCCCC;}\r\n		.emailCalendarMonth{background-color:#205478; color:#FFFFFF; font-family:Helvetica, Arial, sans-serif; font-size:16px; font-weight:bold; padding-top:10px; padding-bottom:10px; text-align:center;}\r\n		.emailCalendarDay{color:#205478; font-family:Helvetica, Arial, sans-serif; font-size:60px; font-weight:bold; line-height:100%; padding-top:20px; padding-bottom:20px; text-align:center;}\r\n		.imageContentText {margin-top: 10px;line-height:0;}\r\n		.imageContentText a {line-height:0;}\r\n		#invisibleIntroduction {display:none !important;} /* Removing the introduction text from the view */\r\n	\r\n		/*FRAMEWORK HACKS & OVERRIDES */\r\n		span[class=ios-color-hack] a {color:#275100!important;text-decoration:none!important;} /* Remove all link colors in IOS (below are duplicates based on the color preference) */\r\n		span[class=ios-color-hack2] a {color:#205478!important;text-decoration:none!important;}\r\n	 span[class=ios-color-hack3] a {color:#8B8B8B!important;text-decoration:none!important;}\r\n		\r\n		.a[href^=\"tel\"], a[href^=\"sms\"] {text-decoration:none!important;color:#606060!important;pointer-events:none!important;cursor:default!important;}\r\n		.mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {text-decoration:none!important;color:#606060!important;pointer-events:auto!important;cursor:default!important;}\r\n	\r\n	\r\n		/* MOBILE STYLES */\r\n		@media only screen and (max-width: 480px){\r\n		/*////// CLIENT-SPECIFIC STYLES //////*/\r\n		body{width:100% !important; min-width:100% !important;}  \r\n		table[id=\"emailHeader\"],\r\n		table[id=\"emailBody\"],\r\n	 table[id=\"emailFooter\"],\r\n		table[class=\"flexibleContainer\"],\r\n		td[class=\"flexibleContainerCell\"] {width:100% !important;}\r\n		td[class=\"flexibleContainerBox\"], td[class=\"flexibleContainerBox\"] table {display: block;width: 100%;text-align: left;}\r\n		\r\n		td[class=\"imageContent\"] img {height:auto !important; width:100% !important; max-width:100% !important; }\r\n		img[class=\"flexibleImage\"]{height:auto !important; width:100% !important;max-width:100% !important;}\r\n		img[class=\"flexibleImageSmall\"]{height:auto !important; width:auto !important;}\r\n	\r\n	\r\n		\r\n		table[class=\"flexibleContainerBoxNext\"]{padding-top: 10px !important;}\r\n		table[class=\"emailButton\"]{width:100% !important;}\r\n		td[class=\"buttonContent\"]{padding:0 !important;}\r\n		td[class=\"buttonContent\"] a{padding:15px !important;}\r\n	\r\n		}\r\n	\r\n		\r\n	\r\n		@media only screen and (-webkit-device-pixel-ratio:.75){\r\n		/* Put CSS for low density (ldpi) Android layouts in here */\r\n		}\r\n	\r\n		@media only screen and (-webkit-device-pixel-ratio:1){\r\n		/* Put CSS for medium density (mdpi) Android layouts in here */\r\n		}\r\n	\r\n		@media only screen and (-webkit-device-pixel-ratio:1.5){\r\n		/* Put CSS for high density (hdpi) Android layouts in here */\r\n		}\r\n		/* end Android targeting */\r\n	\r\n		/* CONDITIONS FOR IOS DEVICES ONLY\r\n	 =====================================================*/\r\n		@media only screen and (min-device-width : 320px) and (max-device-width:568px) {\r\n	\r\n		}\r\n		/* end IOS targeting */\r\n	</style>\r\n</head>\r\n\r\n<body bgcolor=\"#E1E1E1\" leftmargin=\"0\" marginwidth=\"0\" topmargin=\"0\" marginheight=\"0\" offset=\"0\">\r\n	<!-- CENTER THE EMAIL // -->\r\n	<!--\r\n	1.  The center tag should normally put all the\r\n	content in the middle of the email page.\r\n	I added \"table-layout: fixed;\" style to force\r\n	yahoomail which by default put the content left.\r\n\r\n	2.  For hotmail and yahoomail, the contents of\r\n	the email starts from this center, so we try to\r\n	apply necessary styling e.g. background-color.\r\n	-->\r\n	<center style=\"background-color:#E1E1E1;\">\r\n		<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" id=\"bodyTable\" style=\"table-layout: fixed;max-width:100% !important;width: 100% !important;min-width: 100% !important;\">\r\n			<tr>\r\n				<td align=\"center\" valign=\"top\" id=\"bodyCell\">\r\n					<table bgcolor=\"#E1E1E1\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"500\" id=\"emailHeader\">\r\n						<!-- HEADER ROW // -->\r\n						<tr>\r\n							<td align=\"center\" valign=\"top\">\r\n								<!-- CENTERING TABLE // -->\r\n								<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n									<tr>\r\n										<td align=\"center\" valign=\"top\">\r\n											<!-- FLEXIBLE CONTAINER // -->\r\n											<table border=\"0\" cellpadding=\"10\" cellspacing=\"0\" width=\"500\" class=\"flexibleContainer\">\r\n												<tr>\r\n													<td valign=\"top\" width=\"500\" class=\"flexibleContainerCell\">\r\n														<!-- CONTENT TABLE // -->\r\n														<table align=\"left\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n															<tr>\r\n																<td align=\"left\" valign=\"middle\" id=\"invisibleIntroduction\" class=\"flexibleContainerBox\" style=\"display:none !important; mso-hide:all;\"></td>\r\n																<td align=\"right\" valign=\"middle\" class=\"flexibleContainerBox\">\r\n																	<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width:100%;\">\r\n																		<tr>\r\n																			<td align=\"left\" class=\"textContent\"></td>\r\n																		</tr>\r\n																	</table>\r\n																</td>\r\n															</tr>\r\n														</table>\r\n													</td>\r\n												</tr>\r\n											</table>\r\n											<!-- // FLEXIBLE CONTAINER -->\r\n										</td>\r\n									</tr>\r\n								</table>\r\n								<!-- // CENTERING TABLE -->\r\n							</td>\r\n						</tr>\r\n						<!-- // END -->\r\n					</table>\r\n					<table bgcolor=\"#FFFFFF\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"500\" id=\"emailBody\">\r\n						<tr>\r\n							<td align=\"center\" valign=\"top\">\r\n								<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"color:#FFFFFF;\" bgcolor=\"#3498db\">\r\n									<tr>\r\n										<td align=\"center\" valign=\"top\">\r\n											<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"500\" class=\"flexibleContainer\">\r\n												<tr>\r\n													<td align=\"center\" valign=\"top\" width=\"500\" class=\"flexibleContainerCell\">\r\n														<table border=\"0\" cellpadding=\"30\" cellspacing=\"0\" width=\"100%\">\r\n															<tr>\r\n																<td style=\"width: 25%\"></td>\r\n																<td align=\"center\" valign=\"top\" class=\"textContent\">\r\n																	<img class=\"fix\" src=\"companyLogo\" width=\"100%\" style=\"margin: auto; display: block;\" border=\"0\" alt=\"\" />\r\n																</td>\r\n																<td style=\"width: 25%\"></td>\r\n															</tr>\r\n														</table>\r\n														<!-- // CONTENT TABLE -->\r\n													</td>\r\n												</tr>\r\n											</table>\r\n											<!-- // FLEXIBLE CONTAINER -->\r\n										</td>\r\n									</tr>\r\n								</table>\r\n								<!-- // CENTERING TABLE -->\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td align=\"center\" valign=\"top\"></td>\r\n						</tr>\r\n						<tr>\r\n							<td align=\"center\" valign=\"top\">\r\n								<!-- CENTERING TABLE // -->\r\n								<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" bgcolor=\"#F8F8F8\">\r\n									<tr>\r\n										<td align=\"center\" valign=\"top\">\r\n											<br>\r\n											<br>\r\n											<h3 mc:edit=\"header\" style=\"color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left; margin:2% 10%\">Hi CompanyName Employees,</h3>\r\n										</td>\r\n									</tr>\r\n								</table>\r\n								<!-- // CENTERING TABLE -->\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td align=\"center\" valign=\"top\">\r\n								<!-- CENTERING TABLE // -->\r\n								<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n									<tr style=\"padding-top:0;\">\r\n										<td align=\"center\" valign=\"top\">\r\n											<!-- FLEXIBLE CONTAINER // -->\r\n											<!-- // FLEXIBLE CONTAINER -->\r\n										</td>\r\n									</tr>\r\n								</table>\r\n								<!-- // CENTERING TABLE -->\r\n							</td>\r\n						</tr>\r\n						<tr bgcolor=\"#f8f8f8\">\r\n							<td align=\"center\" valign=\"top\">\r\n								<div style=\"text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin:2% 10%\">\r\n									&nbsp;&nbsp;&nbsp;\r\n									<img class=\"fix\" src=\"mailLogo\" width=\"100%\" style=\"margin: auto; display: block;\" border=\"0\" alt=\"\" />\r\n								</b><br>\r\n								</div><br>\r\n\r\n								<div style=\"text-align:justify;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin:2% 10%\">\r\n									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n									You have been invited to submit candidate profiles for the job\r\n\r\n									<!-- <a href=\"FrontendUrl\">FrontendUrl</a> -->\r\n\r\n								</div>\r\n								<div>\r\n									\"jobID - jobTitle\"\r\n								</div>\r\n								<!-- CENTERING TABLE // -->\r\n								<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" bgcolor=\"#F8F8F8\">\r\n									<tr>\r\n										<td align=\"center\" valign=\"top\">\r\n											<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"50%\" class=\"emailButton\" style=\"background-color: #3498DB;\">\r\n												<tr>\r\n													<td align=\"center\" valign=\"middle\"  style=\"padding-top:15px;padding-bottom:15px;padding-right:15px;padding-left:15px;\">	<a style=\"color:#FFFFFF;text-decoration:none;font-family:Helvetica,Arial,sans-serif;font-size:20px;line-height:135%;\" href=\"FrontendUrl\" target=\"_blank\">Click here to view job details</a>\r\n													</td>\r\n												</tr>\r\n											</table>\r\n											<!-- FLEXIBLE CONTAINER // -->\r\n											<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"500\" class=\"flexibleContainer\">\r\n												<tr>\r\n													<td align=\"center\" valign=\"top\" width=\"500\" class=\"flexibleContainerCell\">\r\n														<table border=\"0\" cellpadding=\"30\" cellspacing=\"0\" width=\"100%\">\r\n															<tr>\r\n																<td align=\"center\" valign=\"top\">\r\n																	<!-- CONTENT TABLE // -->\r\n																	<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n																		<tr>\r\n																			<td valign=\"top\" class=\"textContent\">\r\n																				<div mc:edit=\"body\" style=\"text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;\">All the best / Kind regards,\r\n																					<br>The CompanyName Team</div>\r\n																			</td>\r\n																		</tr>\r\n																	</table>\r\n																	<!-- // CONTENT TABLE -->\r\n																</td>\r\n															</tr>\r\n														</table>\r\n													</td>\r\n												</tr>\r\n											</table>\r\n											<!-- // FLEXIBLE CONTAINER -->\r\n										</td>\r\n									</tr>\r\n								</table>\r\n								<!-- // CENTERING TABLE -->\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td align=\"center\" valign=\"top\">\r\n								<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"color:#FFFFFF;\" bgcolor=\"#3498db\">\r\n									<tr>\r\n										<td align=\"center\" valign=\"top\">\r\n											<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"500\" class=\"flexibleContainer\">\r\n												<tr>\r\n													<td align=\"center\" valign=\"top\" width=\"500\" class=\"flexibleContainerCell\">\r\n														<table border=\"0\" cellpadding=\"30\" cellspacing=\"0\" width=\"100%\">\r\n															<tr>\r\n																<td align=\"center\" valign=\"top\" class=\"textContent\"></td>\r\n															</tr>\r\n														</table>\r\n														<!-- // CONTENT TABLE -->\r\n													</td>\r\n												</tr>\r\n											</table>\r\n											<!-- // FLEXIBLE CONTAINER -->\r\n										</td>\r\n									</tr>\r\n								</table>\r\n								<!-- // CENTERING TABLE -->\r\n							</td>\r\n						</tr>\r\n					</table>\r\n					<table bgcolor=\"#E1E1E1\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"500\" id=\"emailFooter\">\r\n						<tr>\r\n							<td align=\"center\" valign=\"top\">\r\n								<!-- CENTERING TABLE // -->\r\n								<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n									<tr>\r\n										<td align=\"center\" valign=\"top\">\r\n											<!-- FLEXIBLE CONTAINER // -->\r\n											<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"500\" class=\"flexibleContainer\">\r\n												<tr>\r\n													<td align=\"center\" valign=\"top\" width=\"500\" class=\"flexibleContainerCell\">\r\n														<table border=\"0\" cellpadding=\"30\" cellspacing=\"0\" width=\"100%\">\r\n															<tr>\r\n																<td valign=\"top\" bgcolor=\"#E1E1E1\">\r\n																	<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;\"></div>\r\n																</td>\r\n															</tr>\r\n														</table>\r\n													</td>\r\n												</tr>\r\n											</table>\r\n											<!-- // FLEXIBLE CONTAINER -->\r\n										</td>\r\n									</tr>\r\n								</table>\r\n								<!-- // CENTERING TABLE -->\r\n							</td>\r\n						</tr>\r\n					</table>\r\n				</td>\r\n			</tr>\r\n		</table>\r\n	</center>\r\n</body>\r\n\r\n</html>');`
-- insert email templates for vendor email notification while adding the job evaluation Suresh end