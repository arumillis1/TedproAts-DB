-- version : 20.1.V001.00  29th Feb,2020
-- alter the reporting_users view table - sharmistha start

CREATE VIEW reporting_users AS SELECT tedpros_user.user_id, tedpros_user.username,tedpros_user.reporting_to,tedpros_user.status,tedpros_user.employee_type, tedpros_employee_type.id,tedpros_employee_type.name,tedpros_role_assign.role_id as role_id,tedpros_role.role_name as role_name, tedpros_role_assign.status as role_status FROM tedpros_user,tedpros_employee_type,tedpros_role_assign,tedpros_role WHERE (tedpros_user.employee_type=tedpros_employee_type.id) AND tedpros_user.user_type = '3' AND tedpros_role_assign.user_id = tedpros_user.user_id and tedpros_role_assign.role_id = tedpros_role.id;

-- alter the reporting_users view table - sharmistha end